// Fill out your copyright notice in the Description page of Project Settings.


#include "TimeFunctions.h"
#include "SocketSubsystem.h"
#include "Common/UdpSocketBuilder.h"

#define NTP_TIMESTAMP_DELTA 2208988800ull 

DEFINE_LOG_CATEGORY(LogTimeFunctions);

static uint32 ntohl(uint32 const net) {
    uint8 data[4] = {};
    memcpy(&data, &net, sizeof(data));

    return ((uint32)data[3] << 0)
         | ((uint32)data[2] << 8)
         | ((uint32)data[1] << 16)
         | ((uint32)data[0] << 24);
}

struct NTPPacket
{

    uint8_t li_vn_mode;      // Eight bits. li, vn, and mode.
                             // li.   Two bits.   Leap indicator.
                             // vn.   Three bits. Version number of the protocol.
                             // mode. Three bits. Client will pick mode 3 for client.

    uint8_t stratum;         // Eight bits. Stratum level of the local clock.
    uint8_t poll;            // Eight bits. Maximum interval between successive messages.
    uint8_t precision;       // Eight bits. Precision of the local clock.

    uint32_t rootDelay;      // 32 bits. Total round trip delay time.
    uint32_t rootDispersion; // 32 bits. Max error aloud from primary clock source.
    uint32_t refId;          // 32 bits. Reference clock identifier.

    uint32_t refTm_s;        // 32 bits. Reference time-stamp seconds.
    uint32_t refTm_f;        // 32 bits. Reference time-stamp fraction of a second.

    uint32_t origTm_s;       // 32 bits. Originate time-stamp seconds.
    uint32_t origTm_f;       // 32 bits. Originate time-stamp fraction of a second.

    uint32_t rxTm_s;         // 32 bits. Received time-stamp seconds.
    uint32_t rxTm_f;         // 32 bits. Received time-stamp fraction of a second.

    uint32_t txTm_s;         // 32 bits and the most important field the client cares about. Transmit time-stamp seconds.
    uint32_t txTm_f;         // 32 bits. Transmit time-stamp fraction of a second.

    // Total: 384 bits or 48 bytes.
};

FDateTime UTimeFunctions::CurrentNetworkTime(ENetworkTimeResult& Branches, FString Server, float TimeoutSeconds)
{
    FDateTime Date;
    if (CurrentNetworkTime_Impl(Server, Date, TimeoutSeconds))
    {
        Branches = ENetworkTimeResult::Success;
        return Date;
    }
    else
    {
        Branches = ENetworkTimeResult::Failed;
        return {};
    }
}

bool UTimeFunctions::CurrentNetworkTime_Impl(FString Server, FDateTime& OutDate, double Timeout)
{
    FAddressInfoResult AddressInfo = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->GetAddressInfo(
        TEXT("pool.ntp.org"), TEXT("ntp"), EAddressInfoFlags::Default, NAME_None, ESocketType::SOCKTYPE_Datagram);
    
    if (AddressInfo.Results.Num() > 0)
    {
        FSocket* Socket = FUdpSocketBuilder(TEXT("NTP"))
            .AsReusable()
            .AsNonBlocking()
            .Build();

        if (!Socket)
        {
            UE_LOG(LogTimeFunctions, Warning, TEXT("Socket creation failed"));
            return false;
        }

        NTPPacket pkt{};
        pkt.li_vn_mode = 0x1B;

        int32 BytesSent = 0;
        bool Sent = Socket->SendTo(reinterpret_cast<uint8*>(&pkt), sizeof(pkt), BytesSent, *AddressInfo.Results[0].Address);
        if (!Sent || BytesSent != sizeof(pkt))
        {
            UE_LOG(LogTimeFunctions, Warning, TEXT("Failed to send %d (of %d) bytes to %s (%s)"), 
                BytesSent, (int)sizeof(pkt), *Server, *AddressInfo.Results[0].Address->ToString(true));
            ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->DestroySocket(Socket);
            return false;
        }

        int32 BytesRead = 0;
        double TimerStart = FPlatformTime::Seconds();
        while (FPlatformTime::Seconds() - TimerStart < Timeout)
        {
            bool Received = Socket->RecvFrom(reinterpret_cast<uint8*>(&pkt), sizeof(pkt), 
                BytesRead, *AddressInfo.Results[0].Address);
            if (Received)
            {
                if (BytesRead == sizeof(pkt))
                {
                    pkt.txTm_s = ntohl(pkt.txTm_s); // Time-stamp seconds.
                    pkt.txTm_f = ntohl(pkt.txTm_f); // Time-stamp fraction of a second.
                    int64 timestamp = pkt.txTm_s - NTP_TIMESTAMP_DELTA;

                    ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->DestroySocket(Socket);

                    OutDate = FDateTime::FromUnixTimestamp(timestamp);
                    return true;
                }
                else
                {
                    UE_LOG(LogTimeFunctions, Warning, TEXT("Received packet with wrong size %d (of %d) bytes from %s (%s)"),
                        BytesSent, (int)sizeof(pkt), *Server, *AddressInfo.Results[0].Address->ToString(true));
                    ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->DestroySocket(Socket);
                    return false;
                }
            }
            FPlatformProcess::Sleep(0.1);
        }

        UE_LOG(LogTimeFunctions, Warning, TEXT("Failed to receive NTP packet from %s (%s), timeout of %f seconds expired"),
            *Server, *AddressInfo.Results[0].Address->ToString(true), (float)Timeout);
        ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->DestroySocket(Socket);
        return false;
    }

    UE_LOG(LogTimeFunctions, Warning, TEXT("Failed to resolve DNS for host %s"), *Server);
    return false;
}
