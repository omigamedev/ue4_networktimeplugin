// Copyright Epic Games, Inc. All Rights Reserved.

#include "NetworkTime.h"
#include "TimeFunctions.h"

#define LOCTEXT_NAMESPACE "FNetworkTimeModule"

DEFINE_LOG_CATEGORY(LogNetworkTimeModule);

void FNetworkTimeModule::StartupModule()
{
#if !(WITH_EDITOR)
    const TCHAR* ConfigGroup = TEXT("NetworkTimePlugin");
    bool AutoExit = false;
    if (GConfig->GetBool(ConfigGroup, TEXT("bAutoExit"), AutoExit, GGameIni))
    {
        if (AutoExit)
        {
            FString ValidDateEnd;
            FString ValidDateStart;
            FDateTime DateStart(1900, 1, 1);
            if (GConfig->GetString(ConfigGroup, TEXT("ValidDateStart"), ValidDateStart, GGameIni))
            {
                if (FDateTime::ParseIso8601(*ValidDateStart, DateStart))
                {
                    UE_LOG(LogNetworkTimeModule, Warning, 
                        TEXT("ValidDateStart time is: %s"), *DateStart.ToString());
                }
                else
                {
                    UE_LOG(LogNetworkTimeModule, Warning,
                        TEXT("Could not parse date (\"%s\") from ValidDateStart in DefaultGame.ini"), *ValidDateStart);
                }
            }
            if (GConfig->GetString(ConfigGroup, TEXT("ValidDateEnd"), ValidDateEnd, GGameIni))
            {
                FDateTime DateEnd;
                if (FDateTime::ParseIso8601(*ValidDateEnd, DateEnd))
                {
                    FDateTime NetworkDate;
                    UE_LOG(LogNetworkTimeModule, Warning,
                        TEXT("ValidDateEnd time is: %s"), *DateEnd.ToString());
                    if (UTimeFunctions::CurrentNetworkTime_Impl(TEXT("pool.ntp.org"), NetworkDate))
                    {
                        UE_LOG(LogNetworkTimeModule, Warning,
                            TEXT("Current network time is: %s"), *NetworkDate.ToString());
                        if (NetworkDate > DateEnd || NetworkDate < DateStart)
                        {
                            UE_LOG(LogNetworkTimeModule, Warning,
                                TEXT("Time expired, shutting down now"));
                            FGenericPlatformMisc::RequestExit(false);
                        }
                    }
                    else
                    {
                        UE_LOG(LogNetworkTimeModule, Warning,
                            TEXT("Could not retrieve network date, shutting down now"));
                        FGenericPlatformMisc::RequestExit(false);
                    }
                }
                else
                {
                    UE_LOG(LogNetworkTimeModule, Warning, 
                        TEXT("Could not parse date (\"%s\") from ValidDateEnd in DefaultGame.ini"), *ValidDateEnd);
                }
            }
            else
            {
                UE_LOG(LogNetworkTimeModule, Warning, 
                    TEXT("bAutoExit is enabled but ValidDateEnd wasn't specified in DefaultGame.ini"));
            }
        }
    }
    else
    {
        UE_LOG(LogNetworkTimeModule, Warning, TEXT("bAutoExit not found in DefaultGame.ini"));
    }
#endif
}

void FNetworkTimeModule::ShutdownModule()
{
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FNetworkTimeModule, NetworkTime)
