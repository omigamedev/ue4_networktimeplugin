// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "TimeFunctions.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogTimeFunctions, Log, All);

UENUM(BlueprintType)
enum class ENetworkTimeResult : uint8
{
    Success,
    Failed
};

UCLASS()
class NETWORKTIME_API UTimeFunctions : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
    UFUNCTION(BlueprintCallable, Meta = (ExpandEnumAsExecs = "Branches"))
    static FDateTime CurrentNetworkTime(ENetworkTimeResult& Branches, FString Server = TEXT("pool.ntp.org"), float TimeoutSeconds = 1.0);
    static bool CurrentNetworkTime_Impl(FString Server, FDateTime& OutDate, double Timeout = 1.0);
};
