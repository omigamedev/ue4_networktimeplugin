// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TimeClientGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TIMECLIENT_API ATimeClientGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
