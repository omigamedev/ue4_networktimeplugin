// Copyright Epic Games, Inc. All Rights Reserved.

#include "TimeClient.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TimeClient, "TimeClient" );
