# NetworkTime Plugin
This is a plugin that queries the current time by connecting to an NTP server.

It then can perform a time range check and shutdown the game if expired.

# CurrentNetworkTime BP
It is a Blueprint node that can be used to retrieve the current time from the specified NTP server.

This makes it possible to perform custom checks.

# Auto shutdown configuration
To enable the auto shutdown just set bAutoExit=True in DefaultGame.ini

The configuration in DefaultGame.ini looks like this:

```

[NetworkTimePlugin]
bAutoExit=True
ValidDateEnd=2021-03-02
ValidDateStart=2021-03-01
DefaultNTPServer=pool.ntp.org
```

Note that the auto shutdown does not work in Editor mode.